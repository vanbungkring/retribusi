var mongoose = require('mongoose');
var bcrypt   = require('crypto');
///permission if 1=admin 2==employe 3=user
var userSchema = mongoose.Schema({
  name: String,
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  permission :Number,
  location: String,
  meta: {
    age: Number,
    website: String
  },
  created_at: { type: Date, default: Date.now }
  updated_at: Date
});
// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema);

UserSchema.pre('save',
    function(next) {
        if (this.password) {
            var md5 = crypto.createHash('md5');
            this.password = md5.update(this.password).digest('hex');
        }
        next();
    }
);
// make this available to our users in our Node applications
module.exports = User;
