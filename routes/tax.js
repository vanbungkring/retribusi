var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/hotel', function (req, res, next) {
	res.render('pajak/hotel/index', {
		title: 'Express'
	});
});
router.get('/hotel/create', function (req, res, next) {
	res.render('pajak/hotel/create', {
		title: 'Pajak reklame'
	});
});
router.get('/reklame/', function (req, res, next) {
	res.render('pajak/reklame', {
		title: 'Pajak Hotel'
	});
});
router.get('/reklame/create', function (req, res, next) {
	res.render('pajak/reklame/create', {
		title: 'Pajak reklame'
	});
});

router.get('/parkir/', function (req, res, next) {
	res.render('pajak/hotel', {
		title: 'Pajak Parkir'
	});
});
router.get('/parkir/create', function (req, res, next) {
	res.render('pajak/hotel/create', {
		title: 'Pajak Parkir'
	});
});

// /* GET home page. */
// router.get('/hotel', function (req, res, next) {
// 	res.render('login', {
// 		title: 'Express'
// 	});
// });

module.exports = router;
