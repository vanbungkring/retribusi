var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
  res.render('company/index', { title: req.body.id });
});
router.get('/:id', function(req, res,next) {
  console.log(req.params.id);
  if (req.params.id =='add') {
    next();
  }
  res.render('company/company', { title: req.params.id });
});

router.get('/add', function(req, res) {
  res.render('company/add', { title: req.params.id });
});module.exports = router;

router.post('/add', function(req, res) {
  res.redirect('/company')
});module.exports = router;
